package com.ideca.encrypt.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EncriptorSimpleTest {

	private final String CLEAR_TEXT = "test@testmail.com";
	private final String ENCODED_TEXT = "dGVzdEB0ZXN0bWFpbC5jb20=";

	@Test
	public void debeEncriptarTexto() {

		String esperado = ENCODED_TEXT;
		String resultado = EncriptorSimple.encriptar(CLEAR_TEXT);

		assertEquals(esperado, resultado);

	}

	@Test
	public void debeDesencriptarTexto() {

		String esperado = CLEAR_TEXT;
		String resultado = EncriptorSimple.desencriptar(ENCODED_TEXT);

		assertEquals(esperado, resultado);

	}

}
