package com.ideca.repositorios;

import org.springframework.data.repository.CrudRepository;

import com.ideca.model.Usuario;

public interface RepositorioUsuario extends CrudRepository<Usuario, Integer> {
	
	Usuario findByCorreo(String email);

}
