package com.ideca.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ideca.model.Mensaje;
import com.ideca.model.Usuario;
import com.ideca.servicios.UsuarioService;

@RestController
public class RegisterController {

	@Autowired
	private UsuarioService usuarioService;

	@RequestMapping(path = "/api/registrar", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<Mensaje> registrarUsuario(@RequestBody Usuario usuarioInput) {

		String resultado = usuarioService.registrarNuevoUsuario(usuarioInput);

		ResponseEntity<Mensaje> response = UsuarioService.RESULTADO_OK.equals(resultado)
				? new ResponseEntity<Mensaje>(
						new Mensaje("Usuario registrado exitosamente. Por favor confirme su correo"), HttpStatus.OK)
				: new ResponseEntity<Mensaje>(
						new Mensaje("Error. El correo ingresado ya se encuentra en nuestro sistema."),
						HttpStatus.BAD_REQUEST);

		return response;
	}

	@RequestMapping(path = "/api/validar", method = RequestMethod.GET)
	public ResponseEntity<String> validarUsuario(@RequestParam String u) {

		if (StringUtils.isEmpty(u)) {
			return new ResponseEntity<String>("Error. No se encuentra correo para confirmar", HttpStatus.BAD_REQUEST);
		}

		String resultado = usuarioService.activarUsuario(u);

		ResponseEntity<String> response = UsuarioService.RESULTADO_OK.equals(resultado)
				? new ResponseEntity<String>("Usuario confirmado exitosamente. Puede cerrar la pestaña", HttpStatus.OK)
				: new ResponseEntity<String>("Error. No existe usuario.", HttpStatus.NOT_FOUND);

		return response;
	}

}
