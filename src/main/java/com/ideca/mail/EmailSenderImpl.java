package com.ideca.mail;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.ideca.encrypt.utils.EncriptorSimple;

@Service
public class EmailSenderImpl implements EmailSender {

	@Autowired
	private JavaMailSender mailSender;

	private String textoTemplate = "<!DOCTYPE html><html><head></head><body><p>Muchas gracias por registrarse en Portal Mapas de Bogot&aacute;.</p>"
			+ "<p>Si usted no ha realizado este proceso, por favor ignore este mensaje,</p>" + "<p>&nbsp;</p>"
			+ "<p>En caso contrario ingese al siguiente link para confirmar su correo:</p>"
			+ "<p>{{link}}</p><p>&nbsp;</p><p>Si la p&aacute;gina no lo redirecciona, copie el link y p&eacute;guelo en una nueva pesta&ntilde;a.</p>"
			+ "<p>&nbsp;</p><p>Muchas gracias.</p><p>&nbsp;</p></body></html>";

	@Value("${ideca.urlConfirmacion}")
	private String urlConfirmacion;

	@Value("${ideca.mail.from}")
	private String from;

	@Override
	@Async
	public void enviarCorreoConfirmacion(String destino) {

		MimeMessage mail = mailSender.createMimeMessage();
		try {

			String token = EncriptorSimple.encriptar(destino);
			String link = urlConfirmacion + token;

			MimeMessageHelper helper = new MimeMessageHelper(mail, true);
			helper.setTo(destino);
			String texto = textoTemplate.replace("{{link}}", link);

			helper.setText(texto, true);
			helper.setFrom(from);
			helper.setSubject("Confirme su usuario");

			mailSender.send(mail);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}

	public void setUrlConfirmacion(String urlConfirmacion) {
		this.urlConfirmacion = urlConfirmacion;
	}

	public void setFrom(String from) {
		this.from = from;
	}

}
