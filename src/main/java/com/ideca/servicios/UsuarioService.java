package com.ideca.servicios;

import com.ideca.model.Usuario;

public interface UsuarioService {

	public static String RESULTADO_OK = "OK";
	public static String RESULTADO_YA_EXISTE = "YA";
	public static String RESULTADO_NO_EXISTE = "NE";

	String registrarNuevoUsuario(Usuario nuevoUsuario);

	String activarUsuario(String correo);

}