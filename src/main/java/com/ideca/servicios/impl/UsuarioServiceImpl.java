package com.ideca.servicios.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideca.encrypt.utils.EncriptorSimple;
import com.ideca.mail.EmailSender;
import com.ideca.model.Usuario;
import com.ideca.repositorios.RepositorioUsuario;
import com.ideca.servicios.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private RepositorioUsuario repositorio;
	@Autowired
	private EmailSender mailSender;

	@Override
	public String registrarNuevoUsuario(Usuario nuevoUsuario) {

		Usuario usuarioConsulta = repositorio.findByCorreo(nuevoUsuario.getCorreo());

		if (usuarioConsulta == null) {
			nuevoUsuario.setEstado(Usuario.INACTIVO);
			repositorio.save(nuevoUsuario);
			mailSender.enviarCorreoConfirmacion(nuevoUsuario.getCorreo());
			return RESULTADO_OK;
		}
		return RESULTADO_YA_EXISTE;

	}

	@Override
	public String activarUsuario(String correoCodificado) {

		String email = EncriptorSimple.desencriptar(correoCodificado);
		Usuario usuario = repositorio.findByCorreo(email);

		if (usuario != null) {
			usuario.setEstado(Usuario.ACTIVO);
			repositorio.save(usuario);
			return RESULTADO_OK;
		}
		return RESULTADO_NO_EXISTE;
	}

}
