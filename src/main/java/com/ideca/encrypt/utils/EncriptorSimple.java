package com.ideca.encrypt.utils;

import org.apache.tomcat.util.codec.binary.Base64;

public class EncriptorSimple {
	
	public static String encriptar(String texto) {
		byte[] bytes = Base64.encodeBase64(texto.getBytes());
		return new String(bytes);
	}

	public static String desencriptar(String texto) {
		byte[] bytes = Base64.decodeBase64(texto.getBytes());
		return new String(bytes);
	}

}
