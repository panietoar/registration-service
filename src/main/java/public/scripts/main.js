/**
 * 
 */
$(document).ready(function() {

	$("form").submit(function(event) {
		event.preventDefault();
		var jsonForm = serializarForm($(this));
		$.ajax({
			method : "POST",
			url : "/api/registrar",
			data : JSON.stringify(jsonForm),
			headers : {
				"Content-Type" : "application/json"
			}
		}).done(function(data, textStatus, xhr) {
			$("#resultado_success").html(data.mensaje);
			$(".panel-success").toggle();
		}).fail(function(data, textStatus, xhr) {
			var respuesta = JSON.parse(data.responseText);
			$("#resultado_error").html(respuesta.mensaje);
			$(".panel-danger").toggle();
		});

	});

});

function serializarForm(formData) {

	var o = {};
	var a = formData.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
}